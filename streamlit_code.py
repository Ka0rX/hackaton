# Commande terminal : streamlit run streamlit_code.py

import streamlit as st
import numpy as np
import pandas as pd
from tensorflow import keras

from io import BytesIO
from numpy import load
from numpy import expand_dims
from matplotlib import pyplot

from PIL import Image, ImageDraw, ImageFont

import os

#from resnet_model import ResnetModel 



def load_model(path): 
	model = keras.models.load_model(path)
	return model
	
def calcul_prediction(image,model):
    
    pred = model.predict(np.array([image]))
    return pred[0][0]
	
	
st.header("Classification de silos")

#-----------------------------------------------------------------------------

st.subheader(" Présentation") 
st.write("Startup (fictif) composée de 50 collaborateurs dont 30 aux US (Des Moines – Iowa)") 
st.write("Proposition de valeur : fabricants de silos connectés")
st.write("Notre mission : s’attaquer au problème de la faim dans le monde en réduisant drastiquement ces pertes alimentaires par l’angle du stockage grâce à nos silos")
st.write("Pour ce faire, à partir d’images satellites, nous voulons détecter des zones dans le monde qui en sont dépourvues afin de savoir où les installer")

#------------------------------------------------------------------------------
image=None
st.subheader("Drag&Drop ") 
uploaded_file = st.file_uploader("Choose an image...") # le truc de D&D 
if uploaded_file is not None:
    image = Image.open(uploaded_file)
checkbox=st.checkbox("Use our default example")
if(checkbox) :
    image = Image.open("C:\\Users\litiw\Downloads\silos_256-0-0--6-14--19-28655.png")

if image is not None:
    st.image(image, caption='image satellite', use_column_width=True)
    	
#----------------------------------------------------------------------------	
if image is not None :
    
    st.subheader("Resultat")  
    img = np.asarray(image)
    model = load_model('model.h5')
    i = calcul_prediction(img,model)
    out=""
    val =np.round(i)
    if(val==0) :
     st.write("Il n'y a pas de silo sur cette image.")
    else :
        st.write("Il y a au moins 1 silo sur cette image.")
    st.download_button('Download data', str(i))
    
#--------------------------------------------------------------------------------------
st.subheader("Statistiques de notre projet") 




